(function () {
	angular
		.module("filmApp", [])
		.controller("filmAppController", omdbController);

	function omdbController($http) {
		var model = this;
		model.searchByTitle = searchByTitle;
		model.searchMovieByImdbID = searchMovieByImdbID;
		model.addToFavourites = addToFavourites;
		var favouritesArray = [];

		function searchMovieByImdbID(imdbID) {
			var url = "http://www.omdbapi.com/?apikey=d4458e16&i=" + imdbID;
			$http.get(url).then(renderMovieDetails);
		}
		function addToFavourites(imdbID) {
			var url = "http://www.omdbapi.com/?apikey=d4458e16&i=" + imdbID;
			$http.get(url).then(addFilmToFavouritesList);
		}
		function searchByTitle(title) {
			var url = "http://www.omdbapi.com/?apikey=d4458e16&s=" + title;
			$http.get(url).then(renderMovies);
		}
		function renderMovies(response) {
			model.movies = response.data.Search;
		}
		function renderMovieDetails(response) {
			model.movie = response.data;
		}
	}
})();
