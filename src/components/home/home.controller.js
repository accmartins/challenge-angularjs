export default class HomeController {
	constructor(
		$log,
		homeService
		) {
		'ngInject';

		this.$log = $log;
		this.homeService = homeService;
	}

	$onInit = () => {
		this.heading = 'Welcome to AngularJS ES6 Starter-Kit';
		this.$log.info('Activated Home View.');
	};
}
