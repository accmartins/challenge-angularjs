import headerComponent from "./header.component";

const headerModule = angular
	.module("app.header", [])
	.controller("searchController", searchController);
function searchController($http, $rootScope) {
	var model = this;
	model.searchByTitle = searchByTitle;
	model.searchMovieByImdbID = searchMovieByImdbID;

	function searchByTitle(title) {
		var url = "http://www.omdbapi.com/?apikey=d4458e16&s=" + title;
		$http.get(url).then(renderMovies);
		/*$http.get(url).then(function (response) {
			console.log(response);
		});*/
	}

	function searchMovieByImdbID(imdbID) {
		$rootScope.movieId = imdbID;

		var url = "http://www.omdbapi.com/?apikey=d4458e16&i=" + imdbID;
		$http.get(url).then(renderMovieDetails);
	}

	function renderMovies(response) {
		model.movies = response.data.Search;
	}

	function renderMovieDetails(response) {
		model.movie = response.data;
	}
}
// loading components, services, directives, specific to this module.
headerModule.component("appHeader", headerComponent);

// export this module
export default headerModule;
