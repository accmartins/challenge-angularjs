// for loading styles we need to load main scss file
import "./styles/styles.scss";

// loading shared module
import "./services/core.module";
// loading all module components
import "./app.components";

const appModule = angular
	.module("angularjs-es6-starter-kit", [
		// shared module
		"app.core",
		// 3rd party modules
		"ui.router",
		// application specific modules
		"app.header",
		"app.card",
		"app.home",
		"app.movie",
	])
	.controller("filmAppController", omdbController);
function omdbController($http) {
	var model = this;
	model.searchByTitle = searchByTitle;
	model.searchMovieByImdbID = searchMovieByImdbID;

	function searchMovieByImdbID(imdbID) {
		var url = "http://www.omdbapi.com/?apikey=d4458e16&i=" + imdbID;
		$http.get(url).then(renderMovieDetails);
	}

	function searchByTitle(title) {
		var url = "http://www.omdbapi.com/?apikey=d4458e16&s=" + title;
		$http.get(url).then(renderMovies);
		/*$http.get(url).then(function (response) {
			console.log(response);
		});*/
	}
	function renderMovies(response) {
		model.movies = response.data.Search;
	}
	function renderMovieDetails(response) {
		model.movie = response.data;
	}
}
export default appModule;
