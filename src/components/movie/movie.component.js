import template from './movie.html';
import controller from './movie.controller';

export default {
	controller: controller,
	template: template
}
