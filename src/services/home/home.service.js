export default class HomeService {
	constructor(
		$http
	) {
		'ngInject';

		this.$http = $http;
	}

	get = (query) => {
		return this.$http.get('http://www.omdbapi.com/?s=' + query + '&apikey=6768af47')
			.then((response) => response.data);
	};
}
