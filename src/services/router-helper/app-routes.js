export default [
	{
		name: 'home',
		url: '/',
		component: 'home'
	},
	{
		name: 'movie',
		url: '/movie',
		component: 'movie'
	}
];
