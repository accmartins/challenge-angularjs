export default class MovieController {
	constructor($log) {
		"ngInject";

		this.$log = $log;
	}

	$onInit = () => {
		var model = this;
		model.heading = "Movie Title";

		this.$log.info("Activated Movie View.");
	};
}
