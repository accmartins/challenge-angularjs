import cardComponent from './card.component';

const cardModule = angular.module('app.card', []);

// loading components, services, directives, specific to this module.
cardModule.component('appCard', cardComponent);

// export this module
export default cardModule;
