import movieComponent from "./movie.component";

const movieModule = angular
	.module("app.movie", [])
	.controller("movieDetailsController", movieDetailsController);
function movieDetailsController($http, $rootScope) {
	var model = this;
	model.searchMovieByImdbID = searchMovieByImdbID;

	//var movieId = $location.queryString; //this is for query string
	var movieId = $rootScope.movieId; //this is from rootScope

	function searchMovieByImdbID(imdbID) {
		var url = "http://www.omdbapi.com/?apikey=d4458e16&i=" + imdbID;
		$http.get(url).then(renderMovieDetails);
	}

	function renderMovieDetails(response) {
		model.movie = response.data;
	}

	searchMovieByImdbID(movieId);
}
// loading components, services, directives, specific to this module.
movieModule.component("movie", movieComponent);

// export this module
export default movieModule;
